let licht = 0
let countdown = 0
countdown = 10
licht = input.lightLevel()
basic.forever(() => {
    if (countdown > 0) {
        countdown += -1
        basic.showString("" + countdown + "")
        basic.pause(1000)
    } else {
        basic.clearScreen()
        licht = input.lightLevel()
        if (licht > 40) {
            basic.showString("Alarm")
        }
    }
})
