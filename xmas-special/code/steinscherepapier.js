let _symbol = 0
input.onGesture(Gesture.Shake, () => {
    _symbol = Math.random(3)
    if (_symbol == 0) {
        basic.showIcon(IconNames.Diamond)
    }
    if (_symbol == 1) {
        basic.showIcon(IconNames.Scissors)
    }
    if (_symbol == 2) {
        basic.showIcon(IconNames.Square)
    }
})
