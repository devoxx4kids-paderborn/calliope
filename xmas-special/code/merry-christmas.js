let knopf = 0
input.onButtonPressed(Button.A, () => {
    if (knopf == 0) {
        knopf = 1
    }
})
basic.forever(() => {
    if (knopf == 1) {
        music.setTempo(56)
        basic.pause(1000)
        for (let index = 0; index <= 1; index++) {
            music.playTone(262, music.beat(BeatFraction.Half))
            music.playTone(349, music.beat(BeatFraction.Half))
            music.playTone(349, music.beat(BeatFraction.Quarter))
            music.playTone(392, music.beat(BeatFraction.Quarter))
            music.playTone(349, music.beat(BeatFraction.Quarter))
            music.playTone(330, music.beat(BeatFraction.Quarter))
            music.playTone(294, music.beat(BeatFraction.Half))
            music.playTone(294, music.beat(BeatFraction.Half))
            music.playTone(294, music.beat(BeatFraction.Half))
            music.playTone(392, music.beat(BeatFraction.Half))
            music.playTone(392, music.beat(BeatFraction.Quarter))
            music.playTone(440, music.beat(BeatFraction.Quarter))
            music.playTone(392, music.beat(BeatFraction.Quarter))
            music.playTone(349, music.beat(BeatFraction.Quarter))
            music.playTone(330, music.beat(BeatFraction.Half))
            music.playTone(262, music.beat(BeatFraction.Half))
            music.playTone(262, music.beat(BeatFraction.Half))
            if (index == 0) {
                music.playTone(440, music.beat(BeatFraction.Half))
                music.playTone(440, music.beat(BeatFraction.Quarter))
                music.playTone(494, music.beat(BeatFraction.Quarter))
                music.playTone(440, music.beat(BeatFraction.Quarter))
                music.playTone(392, music.beat(BeatFraction.Quarter))
                music.playTone(349, music.beat(BeatFraction.Half))
                music.playTone(294, music.beat(BeatFraction.Half))
                music.playTone(262, music.beat(BeatFraction.Quarter))
                music.playTone(294, music.beat(BeatFraction.Half))
                music.playTone(392, music.beat(BeatFraction.Half))
                music.playTone(330, music.beat(BeatFraction.Half))
                music.playTone(349, music.beat(BeatFraction.Whole))
            } else {
                music.playTone(440, music.beat(BeatFraction.Half))
                music.playTone(440, music.beat(BeatFraction.Quarter))
                music.playTone(494, music.beat(BeatFraction.Quarter))
                music.playTone(440, music.beat(BeatFraction.Quarter))
                music.playTone(392, music.beat(BeatFraction.Quarter))
                music.playTone(349, music.beat(BeatFraction.Half))
                music.playTone(294, music.beat(BeatFraction.Half))
                music.playTone(262, music.beat(BeatFraction.Quarter))
                music.playTone(294, music.beat(BeatFraction.Half))
                music.playTone(392, music.beat(BeatFraction.Half))
                music.playTone(330, music.beat(BeatFraction.Half))
                music.playTone(349, music.beat(BeatFraction.Whole))
            }
        }
        basic.pause(1000)
        knopf = 0
    }
})
