basic.forever(() => {
    basic.showLeds(`
        . . . . #
        . . . . #
        . . . . #
        . . . . #
        . . . . #
        `)
    basic.showLeds(`
        . . . # #
        . . . # .
        . . . # #
        . . . # .
        . . . # .
        `)
    basic.showLeds(`
        . . # # #
        . . # . .
        . . # # #
        . . # . .
        . . # . .
        `)
    basic.showLeds(`
        . # # # .
        . # . . .
        . # # # .
        . # . . .
        . # . . .
        `)
    basic.showLeds(`
        # # # . #
        # . . . #
        # # # . #
        # . . . #
        # . . . #
        `)
    basic.showLeds(`
        # # . # #
        . . . # .
        # # . # #
        . . . # #
        . . . # .
        `)
    basic.showLeds(`
        # . # # .
        . . # . #
        # . # # .
        . . # # .
        . . # . #
        `)
    basic.showLeds(`
        . # # . .
        . # . # .
        . # # . .
        . # # . .
        . # . # .
        `)
    basic.showLeds(`
        # # . . .
        # . # . #
        # # . . #
        # # . . #
        # . # . .
        `)
    basic.showLeds(`
        # . . . #
        . # . # .
        # . . # .
        # . . # .
        . # . . #
        `)
    basic.showLeds(`
        . . . # #
        # . # . .
        . . # . .
        . . # . .
        # . . # #
        `)
    basic.showLeds(`
        . . # # .
        . # . . #
        . # . . #
        . # . . #
        . . # # .
        `)
    basic.showLeds(`
        . # # . .
        # . . # .
        # . . # .
        # . . # .
        . # # . .
        `)
    basic.showLeds(`
        # # . . #
        . . # . #
        . . # . #
        . . # . #
        # # . . #
        `)
    basic.showLeds(`
        # . . # .
        . # . # .
        . # . # #
        . # . # .
        # . . # .
        `)
    basic.showLeds(`
        . . # . .
        # . # . .
        # . # # #
        # . # . .
        . . # . .
        `)
    basic.showLeds(`
        . # . . #
        . # . . #
        . # # # #
        . # . . #
        . # . . #
        `)
    basic.showLeds(`
        # . . # .
        # . . # .
        # # # # .
        # . . # .
        # . . # .
        `)
    basic.showLeds(`
        . . # . #
        . . # . #
        # # # . #
        . . # . #
        . . # . #
        `)
    basic.showLeds(`
        . # . # #
        . # . # .
        # # . # #
        . # . # .
        . # . # #
        `)
    basic.showLeds(`
        # . # # #
        # . # . .
        # . # # .
        # . # . .
        # . # # #
        `)
    basic.showLeds(`
        . # # # .
        . # . . .
        . # # . .
        . # . . .
        . # # # .
        `)
})
